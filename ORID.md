### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
Today the teachers introduced the program of the camp and made us familiar with each other by playing games. 
Then they introduced the theory of PCDA, the learning pyramid theory; 
at the same time, they introduced the importance of self-directed learning through a video; 
in the afternoon, we learned about concept maps, and discussed the results in groups through topic exercises; 
we learned about the ORID problem solving framework. 
In the afternoon, we learned about concept map and discussed the results as a group through topic practice. 
We also learned about ORID problem solving framework. What impressed me most was that through a topic,
 we were allowed to explore the answer to the problem independently, 
 from theory to practice, not just limited to record.
### R (Reflective): Please use one word to express your feelings about today's class.
Positive.
### (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?
I think today's course made us look at this training afresh in terms of thinking, 
emphasizing the idea of teacher-assisted and independent learning, 
and the most meaningful part of the course was to use theory plus practice to show us the meaning of independent learning.
### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
I would most like to apply what I have learned today to my future self-study as well as to the team, 
which will serve as a guide for our future work. 
I will follow interest and initiative to change the way I accumulate knowledge.